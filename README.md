# lom-quiz

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

# Last chance for humanity

##Introduction

  Many years ago, the world became dominated by machines after the most ferocious war humanity had ever seen.


  Right now, you are one of the few humans who stays alive, and the leader of the machines (a.k.a. LOM) brings you the opportunity to travel back in time to the moment when everything changed.
<p>
It is up to you to please LOM to earn another chance for humanity.
LOM only ask you to do one thing:
Build a program that tests the human's value.
</p>
<p>
  For doing this, LOM brings you a document with the specific requirements of the program, which content is:
    <ul>
      <li>
        The program must ask the user at least ten questions with only one correct answer.
      </li>
      <li>
        The questions can be of any kind: a calculation, choose one from multiple choices, text answer, or whatever. But always with a single correct answer.<br>For example:
        <ul>
          <li><i>"What is the current year?"</i> with a number input.</li>
          <li><i>"12 / 4 ="</i> and a selector with options 4, 3, 5.</li>
          <li><i>"What is the surname of the leading creator of relativity theory called Albert?"</i> with a text inputfor type Einstein</li>
        </ul>
      </li>
      <li>
        The user will have only one chance to answer each question.
      </li>
      <li>
        There must be feedback for the user's answer, and if it's wrong, the program must display the correct answer.
      </li>
      <li>
        The program must track user progress to display the answered questions.
      </li>
      <li>
        When the user answers all the questions, the program must display the result of the tests giving him a value of his worthiness.
      </li>
  </ul>

<p>
  If the program fits the requirements, humanity will be honored with another chance to prove its value; otherwise, the rest of humanity will be annihilated, and the machines will reign the world forever.
</p>

###Technical notes

<p>
  You can do the exercise as you want; you can use any library or framework to do it; also, you can do it with plain HTML, CSS, and JS.
  <br>
  You also can improve styles, usability, or performance as far as you want, but always fitting LOM requirements.
<p>
  To develop the program, you can fork this codepen, create a new one, or even create a new repo on any platform (GitHub, GitLab, etc.) and share it with us.
</p>
<p>
  The requirement here is to use frontend technologies, and the code must be visible and executable so that we can see the result.
</p>



<hr>
<hr>
<hr>
<hr>



# Última oportunidad para la humanidad

##Introducción

Hace muchos años, el mundo quedó dominado por máquinas después de la guerra más feroz que jamás haya visto la humanidad.


En este momento, eres uno de los pocos humanos que permanece con vida, y el líder de las máquinas (también conocido como LOM) te brinda la oportunidad de viajar en el tiempo hasta el momento en que todo cambió.
<p>
Depende de ti complacer a LOM para ganar otra oportunidad para la humanidad.
LOM solo te pide que hagas una cosa:
Desarrolle un programa que pruebe el valor del ser humano.
</p>
<p>
  Para ello, LOM te trae un documento con los requisitos específicos del programa, cuyo contenido es:
    <ul>
      <li>
        El programa debe hacer al usuario al menos diez preguntas con una sola respuesta correcta.
      </li>
      <li>
        Las preguntas pueden ser de cualquier tipo: un cálculo, elegir una entre múltiples opciones, respuesta de texto o lo que sea. Pero siempre con una única respuesta correcta. <br> Por ejemplo:
        <ul>
          <li> <i> "¿Cuál es el año actual?" </i> con una entrada numérica. </li>
          <li> <i> "12/4 =" </i> y un selector con las opciones 4, 3, 5. </li>
          <li> <i> "¿Cuál es el apellido del principal creador de la teoría de la relatividad llamado Albert?" </i> con una entrada de texto para el tipo Einstein </li>
        </ul>
      </li>
      <li>
        El usuario solo tendrá una oportunidad de responder a cada pregunta.
      </li>
      <li>
        Debe haber retroalimentación para la respuesta del usuario y, si es incorrecta, el programa debe mostrar la respuesta correcta.
      </li>
      <li>
        El programa debe realizar un seguimiento del progreso del usuario para mostrar las preguntas respondidas.
      </li>
      <li>
        Cuando el usuario responde todas las preguntas, el programa debe mostrar el resultado de las pruebas dándole un valor de su valía.
      </li>
  </ul>

<p>
  Si el programa cumple con los requisitos, la humanidad será honrada con otra oportunidad de demostrar su valor; de lo contrario, el resto de la humanidad será aniquilado y las máquinas reinarán el mundo para siempre.
</p>

### Notas técnicas

<p>
  Puedes hacer el ejercicio como quieras; puede usar cualquier biblioteca o marco para hacerlo; Además, puede hacerlo con HTML, CSS y JS sin formato.
  por
  También puede mejorar los estilos, la usabilidad o el rendimiento tanto como desee, pero siempre ajustándose a los requisitos de LOM.
<p>
  Para desarrollar el programa, puede bifurcar este codepen, crear uno nuevo o incluso crear un nuevo repositorio en cualquier plataforma (GitHub, GitLab, etc.) y compartirlo con nosotros.
</p>
<p>
  El requisito aquí es usar tecnologías frontend, y el código debe ser visible y ejecutable para que podamos ver el resultado.
</p>