import {createStore} from 'vuex'

export default createStore({
    state: {
        question_counter: 0,
        questions: [
            {
                question: '¿Qué animal tiene cuatro patas, ladra y es el mejor amigo del hombre?',
                answer: 'perro',
                user_answer: '',
                is_good: null
            },
            {
                question: '¿Cuántos minutos tiene una hora?',
                answer: '60',
                user_answer: '',
                is_good: null
            },
            {
                question: '¿Cuántas patas tiene una araña?',
                answer: '8',
                user_answer: '',
                is_good: null
            },
            {
                question: '¿Cuál es la capital de Italia?',
                answer: 'Roma',
                user_answer: '',
                is_good: null
            },
            {
                question: '¿Cómo se llama la ciencia que estudia los astros?',
                answer: 'Astronomía',
                user_answer: '',
                is_good: null
            },
            {
                question: '¿Cuál es el resultado de 3 x 9?',
                answer: '27',
                user_answer: '',
                is_good: null
            },
            {
                question: '¿En qué continente se encuentra España?',
                answer: 'Europa',
                user_answer: '',
                is_good: null
            },
            {
                question: '¿Cuál es la capital de España?',
                answer: 'Madrid',
                user_answer: '',
                is_good: null
            },
            {
                question: '¿Cual es el resultado de 10 / 2?',
                answer: '5',
                user_answer: '',
                is_good: null
            },
            {
                question: '¿Cómo se llama el polígono de tres lados?',
                answer: 'triángulo',
                user_answer: '',
                is_good: null
            },
        ]
    },
    mutations: {
        nextQuestion(state) {
            state.question_counter++
        },
        setUserAnswer(state, payload) {
            if (state.questions[state.question_counter].answer.toLowerCase() === payload.toLowerCase()) {
                state.questions[state.question_counter].is_good = true
            } else {
                state.questions[state.question_counter].is_good = false
            }
            state.questions[state.question_counter].user_answer = payload
        },
        clearData(state) {
            state.question_counter = 0
            state.questions = [
                {
                    question: '¿Qué animal tiene cuatro patas, ladra y es el mejor amigo del hombre?',
                    answer: 'perro',
                    user_answer: '',
                    is_good: null
                },
                {
                    question: '¿Cuántos minutos tiene una hora?',
                    answer: '60',
                    user_answer: '',
                    is_good: null
                },
                {
                    question: '¿Cuántas patas tiene una araña?',
                    answer: '8',
                    user_answer: '',
                    is_good: null
                },
                {
                    question: '¿Cuál es la capital de Italia?',
                    answer: 'Roma',
                    user_answer: '',
                    is_good: null
                },
                {
                    question: '¿Cómo se llama la ciencia que estudia los astros?',
                    answer: 'Astronomía',
                    user_answer: '',
                    is_good: null
                },
                {
                    question: '¿Cuál es el resultado de 3 x 9?',
                    answer: '27',
                    user_answer: '',
                    is_good: null
                },
                {
                    question: '¿En qué continente se encuentra España?',
                    answer: 'Europa',
                    user_answer: '',
                    is_good: null
                },
                {
                    question: '¿Cuál es la capital de España?',
                    answer: 'Madrid',
                    user_answer: '',
                    is_good: null
                },
                {
                    question: '¿Cual es el resultado de 10 / 2?',
                    answer: '5',
                    user_answer: '',
                    is_good: null
                },
                {
                    question: '¿Cómo se llama el polígono de tres lados?',
                    answer: 'triángulo',
                    user_answer: '',
                    is_good: null
                },
            ]
        }
    },
    actions: {},
    modules: {},
    getters: {}
})
